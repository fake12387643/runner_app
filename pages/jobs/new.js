import Head from 'next/head';

import Select from 'react-select';
import * as imm from 'immutable';

const options = [
    {value: 'chocolate', label: 'Chocolate'},
    {value: 'strawberry', label: 'Strawberry'},
    {value: 'vanilla', label: 'Vanilla'}
]

function renderParamGui(typespec, state, setState, validation) {
    const taip = typespec._type;
    if (taip === "SmartParams") {
        const {name, fields} = typespec;

        if (!state) { state = imm.Map({}); }
        if (!validation) { validation = imm.Map({}); }
        return <div>
            {Object.keys(fields).map((x, idx) =>
                <div key={idx} style={{display: "flex", margin: '10px',
                    backgroundColor: validation.get(x) === true ? 'lightpink' : ''}}>
                    <div style={{margin: '10px'}}>{x}</div>
                {renderParamGui(
                    fields[x],
                    state.get(x, null),
                    newSubstate => setState(state.set(x, newSubstate)),
                    validation.get(x, null))}
                </div>)}</div>;

    }
    else if (taip === "Enum") {
        return <div style={{width: "300px"}}><Select

            options={typespec.vals.map((x) => ({value: x, label: x}))}
            value={state ? {value: state, label: state} : {value: typespec.default, label: typespec.default}}
            defaultValue={{value: typespec.default, label: typespec.default}}
            onChange={newSelection => {setState(newSelection.value)}} />{validation}</div>
    }
    else if (taip === "Predicated" && typespec.name === "PositiveInt") {
        return <span>
            <input type='number' placeholder={typespec.default} value={state || ""} onChange={e => setState(parseInt(e.target.value))} />
            {validation}
        </span>
    }
    else if (taip === "Predicated" && typespec.name === "Optional[PositiveInt]") {
        return <input type='number' placeholder={typespec.default} value={state || ""} onChange={e => setState(parseInt(e.target.value))}  />
    }
    else if (taip === "Dependent") {
        return <div><p>with condition: <code>{typespec.src}</code></p>
            {renderParamGui(typespec.underlying_type, state, setState)}
        </div>
    }
    else if (taip === "type" && typespec.name === "<class 'str'>") {
        return <input value={state || ""} placeholder={typespec.default} onChange={e => setState(e.target.value)}/>
    }
    else if (taip === "type" && typespec.name === "<class 'int'>") {
        return <input type='number' value={state} placeholder={typespec.default} onChange={e => setState(parseInt(e.target.value))} />
    }
    else {
        return <div style={{backgroundColor: "red"}}>{JSON.stringify(typespec)}</div>
    }
}

// function  renderParamGui(typespec, state, setState, validation) {
//     return <div>sdfg</div>
// }

function makeParamSelectState (thing) {
    return imm.Map({});
    // if (thing._type === "SmartParams") {
    //     return imm.Map(thing.fields).map(makeParamSelectState);
    // } else {
    //     return thing.default;
    // }
}

export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {paramState: imm.Map({}), selectedBranch: 'master', createdJobs: imm.List([])};
        fetch('/api/curr_branches')
            .then((response) => response.json())
            .then((data) => {
                this.setState({branches: data});
            })


        // fetch('/api/job_type_specs?dir=/home/buck/repos/runner_app')
        //     .then((response) => response.json())
        //     .then((data) => this.setState({jobs: data}))
    }

    validate() {
        fetch("/api/validate", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "params": this.state.jobParamSelectState,
                "commit": this.state.selectedBranch.value.commit.id,
                "job_file": this.state.jobs[this.state.selectedJob.value].source
            })
        }).then((response) => response.json())
            .then((data) => {
                console.log(data);
                this.setState({validation: imm.fromJS(data)});
            }) // TODO use this
    }

    run() {
        const jobData = {
            "params": this.state.jobParamSelectState,
            "commit": this.state.selectedBranch.value.commit.id,
            "job_file": this.state.jobs[this.state.selectedJob.value].source
        };

        fetch("/api/run", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(jobData)
        }).then((response) => response.json())
            .then((data) => {
                jobData['run_info'] = data;
                this.setState({createdJobs: this.state.createdJobs.push([jobData])})
            }) // TODO use this
    }


    render() {
        return (
            <div className="container">
                <Head>
                    <title>Job app: new job</title>
                    <link rel="icon" href="/favicon.ico"/>
                </Head>

                <main>
                    <h1>Create new job</h1>
                    {this.state.branches &&
                        <Select
                            autoFocus
                            value={this.state.selectedBranch}
                            onChange={newSelection => {
                                this.setState(
                                    {selectedJob: null, selectedBranch: newSelection});
                                const newId = newSelection.value.commit.id;
                                fetch('/api/job_type_specs?commit=' + newId)
                                    .then((response) => response.json())
                                    .then((data) =>
                                        this.setState({jobs: data})
                                    )
                            }}

                            options={this.state.branches.map((x, idx) => ({value: x, label: x.name}))}
                        />}

                    {this.state.jobs && <Select
                        value={this.state.selectedJob}
                        onChange={newSelection => this.setState(
                            {selectedJob: newSelection,
                            jobParamSelectState: makeParamSelectState(this.state.jobs[newSelection.value].type_spec)})}
                        options={this.state.jobs.map((x, idx) => ({value: idx, label: x.name + " " + x.source}))}/>}
                    {this.state.selectedJob && <div onBlur={() => this.validate()}>
                        {renderParamGui(this.state.jobs[this.state.selectedJob.value].type_spec, this.state.jobParamSelectState,
                            newVal => this.setState({jobParamSelectState: newVal}),
                            this.state.validation)}
                        {false && <button onClick={() => this.handleSubmit()} key={2}>Submit</button>}
                        </div>
                    }
                    {/*<div>{this.state.jobParamSelectState && JSON.stringify(this.state.jobParamSelectState)}</div>*/}
                    <button onClick={() => this.run()}>Run</button>

                    {!!this.state.createdJobs.size && <div>
                        <h2>Created jobs</h2>
                        {this.state.createdJobs.map((x, idx) => <div key={idx}>{JSON.stringify(x)}</div>)}
                    </div>}
                </main>

                <footer>
                </footer>


            </div>
        )

    }
}


