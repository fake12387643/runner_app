import { withRouter } from 'next/router'
import Head from "next/head";

import { useRouter } from 'next/router'

export default function Page() {
  const router = useRouter()

  return <JobShow jobId={router.query.jobId} />
}

class JobShow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {jobId: props.jobId};
        console.log(props.jobId)

    }

    componentDidMount () {
        this.interval = setInterval(() => {
            if (this.props.jobId) {
                fetch('/api/job?container_name=' + this.props.jobId)
                    .catch(() => {
                    })
                    .then((response) => response.json())
                    .then((data) => {
                        this.setState({jobInfo: data});
                    })
            }
        }, 1000)

    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }


    render () {
        if (this.state) {
            const { jobId } = this.props;
            const { jobInfo } = this.state;

            return <div className="container">
                    <Head>
                        <title>Job app: job {JSON.stringify(jobId)}</title>
                        <link rel="icon" href="/favicon.ico"/>
                    </Head>

                    <main>
                        <h1>{jobId}</h1>
                        {!!jobInfo && <div>
                            <pre>{jobInfo.logs}</pre>
                            {/*JSON.stringify(jobInfo)*/}
                        </div>}
                    </main>
            </div>

        } else {
            return <p>loading</p>
        }
    }

}

// export default withRouter(JobShow)