Current TODOs:

Critical path:

- Handle non-present values, and values without defaults
- Fix bug with enums failing validation
- make a way of actually running the things
- make job viewer

various things:

- make the alternative UI version
- initialize the UI to match old jobs