import json
import sys
from abc import ABC, abstractmethod
from enum import Enum
from runner_py.smart_params import SmartParams, PositiveInt, smartparamify, Optional, Dependent
import typing

class Job(ABC):
    def __init__(self, params: typing.Optional[SmartParams] = None):
        self.params = params if params is not None else self.param_class()

    @property
    @classmethod
    @abstractmethod
    def param_class(cls):
        return NotImplementedError

    @abstractmethod
    def run(self):
        pass

    @classmethod
    def main(cls):
        if len(sys.argv) == 1:
            print("to run, provide a command like type_spec, validate, or run")
        else:
            cmd = sys.argv[1]
            if cmd == "type_spec":
                print(json.dumps({"name": cls.__name__, "type_spec": cls.param_class.to_type_specification(None)}))
            elif cmd == "validate":
                loaded = json.loads(sys.argv[2])
                print(json.dumps(cls.param_class.from_dict(loaded, should_validate=False).validation_errors()))
            elif cmd == "run":
                loaded = json.loads(sys.argv[2])
                params = cls.param_class.from_dict(loaded)
                cls(params).run()
