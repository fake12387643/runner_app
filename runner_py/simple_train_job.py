import time
from enum import Enum, IntEnum

from runner_py.job import Job
from runner_py.smart_params import smartparamify, SmartParams, PositiveInt, Dependent, Optional

NonlinearityType = IntEnum("NonlinearityType", "ReLU ELU SIGMOID")

@smartparamify
class ModelParams(SmartParams):
    depth: PositiveInt = 1
    depth2: Dependent(PositiveInt, lambda self: self.depth >= self.depth2) = 1
    width: PositiveInt = 64
    nonlin_type: NonlinearityType = NonlinearityType.ReLU

    def train_set_size(self):
        return self.parent.task.train_set_size

@smartparamify
class TaskParams(SmartParams):
    train_set_size: Optional[PositiveInt] = None

@smartparamify
class JobParams(SmartParams):
    model: ModelParams = ModelParams()
    task: TaskParams = TaskParams()
    name: str = ""


class SimpleTrainJob(Job):
    param_class = JobParams

    def run(self):
        print(self.params.to_dict())
        print(self.params.name)

        for i in range(100):
            time.sleep(1)
            print(i)

if __name__ == '__main__':
    SimpleTrainJob.main()