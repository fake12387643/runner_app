from runner_py.job import Job
from runner_py.smart_params import smartparamify, SmartParams


@smartparamify
class OtherJob2Params(SmartParams):
    area: float
    height: float

class OtherTrainJob2(Job):
    param_class = OtherJob2Params

    def run(self):
        print(self.params.to_dict())


if __name__ == '__main__':
    OtherTrainJob2.main()