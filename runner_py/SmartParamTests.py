import unittest
from enum import Enum

import jsonpickle

from runner_py.smart_params import smartparamify, PositiveInt, Dependent, PositiveFloat, SmartParams

@smartparamify
class Bar(SmartParams):
    x: PositiveInt = 5
    y: Dependent(PositiveFloat, lambda self: self.x > self.y) = 3.2


MyEnum = Enum("MyEnum", "Enum1 Enum2")


@smartparamify
class Foo(SmartParams):
    bar: Bar = Bar()
    baz: int = 0
    enum: MyEnum = MyEnum.Enum1

class SmartParamTests(unittest.TestCase):
    def test_something(self):
        bar1 = Bar(x = 8)
        self.assertEqual(bar1.x, 8)
        self.assertEqual(bar1.y, 3.2)

        with self.assertRaises(AssertionError):
            Bar(x=4, y=3)
        with self.assertRaises(AssertionError):
            Bar(x=4, y=12.5)

    def test_from_dict(self):
        foo = Foo.from_dict({"baz": -4, "bar": {"x": 4, "y": 2.3}, 'enum': 'Enum1'})
        self.assertEqual(foo.to_dict(), {'bar': {'x': 4, 'y': 2.3}, 'baz': -4, 'enum': MyEnum.Enum1})

    def test_something(self):
        foo = Foo.from_dict({"baz": -4, "bar": {"x": 4, "y": 2.3}, 'enum': 'Enum1'})
        print(foo.to_dict())


if __name__ == '__main__':
    unittest.main()
