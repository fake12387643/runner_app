import ast
import enum
import inspect
import re
from enum import Enum
from typing import Union

import typing


class Predicated:
    def __init__(self, name, taip, pred):
        super().__init__()
        self.name = name
        self.taip = taip
        self.pred = pred

    def check_is_instance(self, val):
        return isinstance(val, self.taip) and self.pred(val)

    def to_type_specification(self, default):
        return dict(_type="Predicated", name=self.name, underlying_type=type_to_type_spec(self.taip),
                    src=inspect.getsource(self.pred).strip(),
                    default=default)

PositiveInt = Predicated("PositiveInt", int, lambda x: x > 0)
PositiveFloat = Predicated("PositiveFloat", float, lambda x: x > 0)



class Dependent:
    def __init__(self, taip, pred):
        self.taip = taip
        self.pred = pred

    def _check_is_instance(self, val):
        checker_fn = getattr(self.taip, "check_is_instance", None)
        if checker_fn:
            return checker_fn(val)
        else:
            return isinstance(val, self.taip)

    def check_is_instance_with_context(self, val, obj):
        return self._check_is_instance(val) and self.pred(obj)

    def to_type_specification(self, default):
        return dict(_type="Dependent", underlying_type=type_to_type_spec(self.taip),
                    src=inspect.getsource(self.pred).strip(),
                    default=default)

def optional_getitem(taip: Union[type, Predicated, Dependent]):
    if isinstance(taip, type):
        return typing.Optional[taip]
    elif isinstance(taip, Predicated):
        return Predicated(f"Optional[{taip.name}]", object,
                          lambda x: True if x is None else taip.check_is_instance(x))
    elif isinstance(taip, Dependent):
        raise NotImplementedError # do I really want to do this?

class OptionalClass:
    @staticmethod
    def __getitem__(taip: Union[type, Predicated, Dependent]):
        if isinstance(taip, type):
            return typing.Optional[taip]
        elif isinstance(taip, Predicated):
            return Predicated(f"Optional[{taip.name}]", object,
                              lambda x: True if x is None else taip.check_is_instance(x))
        elif isinstance(taip, Dependent):
            raise NotImplementedError # do I really want to do this?
Optional = OptionalClass()


def get_args_for_smartparam_class(klass):
    return {k: (v, klass.__annotations__.get(k))
         for k, v in klass.__dict__.items()
         if k[:2] != "__" and k != "check" and not inspect.isfunction(v)}


def smartparamify(klass):
    args = get_args_for_smartparam_class(klass)
    setattr(klass, "_args", args)
    return type(klass.__name__, (SmartParams,), klass.__dict__.copy())

def check_is_instance(val, taip, obj):
    if hasattr(taip, "check_is_instance_with_context"):
        return taip.check_is_instance_with_context(val, obj)
    else:
        if hasattr(taip, "check_is_instance"):
            return taip.check_is_instance(val)
        else:
            return isinstance(val, taip)

class SmartParams:
    # TODO: metaclasses hackery so that you don't need the decorator


    def __init__(self, _should_validate=True, **kwargs):
        args = self.__class__._args
        # TODO: check that kwargs doesn't have any extra things in it
        # TODO: check that every compulsary val has been set
        for fieldname, (default_val, taip) in args.items():
            val = kwargs.get(fieldname, default_val)
            setattr(self, fieldname, val)
            # smart params are able to access their parent via the `_parent` field
            if isinstance(val, SmartParams):
                getattr(self, fieldname)._parent = self

        if _should_validate:
            for fieldname in args:
                assert check_is_instance(getattr(self, fieldname), args[fieldname][1], self), fieldname

    def validation_errors(self):
        args = self.__class__._args

        def sub_errors(fieldname, v):
            if isinstance(v, SmartParams):
                return v.validation_errors()
            else:
                return not check_is_instance(v, args[fieldname][1], self)

        return {field_name: sub_errors(field_name, getattr(self, field_name))
                      for field_name in args}
        # return {k: v for k, v in val_errors.items() if v}

    def to_dict(self):
        def nested_to_dict(x):
            if isinstance(x, SmartParams):
                return x.to_dict()
            else:
                return x
        return {arg: nested_to_dict(getattr(self, arg)) for arg in self.__class__._args}

    @classmethod
    def cli_parse(cls, args: typing.List[str]):
        params = cls()

        assert len(args) % 2 == 0, "cli needs an even number of args"
        for i in range(0, len(args), 2):
            key = args[i]
            val = args[i + 1]

            # TODO: support loading from files and HTTP requests
            if key[:2] == "--":
                params.set_via_path_str(key[2:], ast.literal_eval(val))
            else:
                raise ValueError

        return params

    @classmethod
    def from_dict(cls, in_dict: typing.Dict, should_validate=True):
        def maybe_parse(k, v):
            taip = cls.__annotations__[k]
            if taip and type(taip) == type:
                if issubclass(taip, SmartParams):
                    return taip.from_dict(v, should_validate=should_validate)
            if taip and type(taip) == enum.EnumMeta:
                return taip[v]
            else:
                return v
        parsed_args = {k: maybe_parse(k, v) for k, v in in_dict.items()}
        return cls(_should_validate=should_validate, **parsed_args)


    @classmethod
    def to_type_specification(cls, default):
        return {"_type": "SmartParams", "name": cls.__name__, "fields": {
            k: type_to_type_spec(v, cls._args.get(k, [None])[0])
            for k, v in cls.__annotations__.items()
        }}

    def __getitem__(self, item):
        return getattr(self, item)

    def __setitem__(self, item, val):
        return setattr(self, item, val)

    def __contains__(self, item):
        return hasattr(self, item)

    def set_via_path_str(self, path_name, val):
        # TODO: test the shit out of this
        assert re.match("^(\w+)(([.]\w+)|(\[\w+\]))*$", path_name)
        path_steps = re.findall("[\[]?\w+", path_name)

        curr = self
        for step in path_steps[:-1]:
            if step[0] == "[":
                step = ast.literal_eval(step[1:])
            curr = curr[step]
        curr[path_steps[-1]] = val



def type_to_type_spec(v, default=None):
    if hasattr(v, "to_type_specification"):
        return v.to_type_specification(default)
    elif issubclass(v, Enum):
        return {"_type": "Enum", "vals": [x.name for x in v], "name": v.__name__, "default": default.name}
    else:
        return {"_type": "type", "name": repr(v), "default": default}
