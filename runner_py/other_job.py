from runner_py.job import Job
from runner_py.smart_params import smartparamify, SmartParams


@smartparamify
class OtherJobParams(SmartParams):
    area: float
    height: float

class OtherTrainJob(Job):
    param_class = OtherJobParams

    def run(self):
        print(self.params.to_dict())


if __name__ == '__main__':
    OtherTrainJob.main()