import importlib
import glob
import inspect
import json
import os
import subprocess
import requests
from typing import Dict

DIR = "/home/buck/repos/other_runner_app/runner_app/"
PYTHON_VERSION = "/home/buck/repos/other_runner_app/env/bin/python3"

# def get_job_type_specs(filepath):
#     # we need to make this be a CLI flag instead
#     spec = importlib.util.spec_from_file_location("my_module",
#                                                   DIR + filepath)
#     module = importlib.util.module_from_spec(spec)
#
#     spec.loader.exec_module(module)
#
#     return {k: v.param_class.to_type_specification(None) for k, v in inspect.getmembers(module)
#      if inspect.isclass(v) and any(ancestor.__name__ == "Job" for ancestor in v.mro())
#             and v.__module__ == "my_module"}
#
# def get_job_type_specs_for_dir(dir):
#     job_filepaths = [os.path.relpath(filepath, dir) for filepath in glob.glob(dir + "/**/*_job.py", recursive=True)]
#     out = []
#     for filepath in job_filepaths:
#         for k, v in get_job_type_specs(filepath).items():
#             out.append({"source": filepath, "name": k, "type_spec": v})
#     return out

def get_job_type_specs_given_commit(commit: str):
    subprocess.call(f"git checkout {commit} --force", shell=True, cwd=DIR)
    job_filepaths = glob.glob(DIR + "/**/*_job.py", recursive=True)

    return [dict(**json.loads(subprocess.check_output([PYTHON_VERSION, filepath, "type_spec"])),
                 source=os.path.relpath(filepath, DIR))
            for filepath in job_filepaths]

def validate(commit: str, job_file: str, params: Dict):
    subprocess.call(f"git checkout {commit} --force", shell=True, cwd=DIR)
    return json.loads(subprocess.check_output([PYTHON_VERSION, DIR + job_file, "validate", json.dumps(params)]))

# just for testing purposes
def run(commit: str, job_file: str, params: Dict):
    subprocess.call(f"git checkout {commit} --force", shell=True, cwd=DIR)
    return subprocess.check_output([PYTHON_VERSION, job_file, "run", json.dumps(params)])