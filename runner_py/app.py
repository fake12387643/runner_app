import json
import pipes
import subprocess
import random

import requests
from flask import Flask
from flask import request, jsonify

from runner_py import code_analysis

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/api/job_type_specs')
def api_job_type_specs():
    return jsonify(code_analysis.get_job_type_specs_given_commit(request.args.get('commit')))


@app.route('/api/curr_branches')
def api_curr_branches():
    return jsonify(
        requests.get('https://gitlab.com/api/v4/projects/fake12387643%2frunner_app/repository/branches').json())


@app.route('/api/validate', methods=["POST"])
def api_validate():
    json = request.json
    commit = json['commit']
    job_file = json['job_file']
    params = json['params']
    return jsonify(code_analysis.validate(commit, job_file, params))

@app.route('/api/run', methods=["POST"])
def api_run():
    req_json = request.json
    commit = req_json['commit']
    job_file = req_json['job_file']
    params = req_json['params']

    container_name = f"job{random.randint(0, 100000000)}"
    subprocess.Popen(f"docker run -it --name {container_name} -d python bash -c".split(" ") +
                     ["cd && git clone https://gitlab.com/fake12387643/runner_app &&  cd runner_app && "
                      f"git checkout {commit} &&"
                      "python3 setup.py develop &&"
                      
                      f"python3 {job_file} "
                      "run "
                      + pipes.quote(json.dumps(params))])
    return jsonify({"container_name": container_name})


@app.route('/api/job')
def api_job():
    container_name = request.args.get('container_name')
    return jsonify({"logs": subprocess.check_output(["docker", "logs", container_name])})


if __name__ == '__main__':
    app.run()
